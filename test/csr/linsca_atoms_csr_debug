#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_atoms_csr_debug.info <<'%EOF%'
   linsca_atoms_csr_debug
   -------------
   Molecule:         HeH+
   Wave Function:    HF/STO-3G,Ahlrichs-Coulomb-Fit
   Test Purpose:     Check CSR library
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_atoms_csr_debug.mol <<'%EOF%'
BASIS
STO-3G !Aux=Ahlrichs-Coulomb-Fit
HeH, CCSD(T)/cc-pVQZ opt. geometry
Test CSR library
Atomtypes=2 Charge=+1 Nosymmetry Angstrom
Charge=1.0 Atoms=1
H     0.0000000000   0.0000000000   0.0000000000
Charge=2.0 Atoms=1
He    0.0000000000   0.0000000000   3.9218840000
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_atoms_csr_debug.dal <<'%EOF%'
**GENERAL
.CSR
.ZERO
1.d-8
**WAVE FUNCTIONS
.HF
*DENSOPT
.2ND_LOC
.NOAV
.START
H1DIAG
! ATOMS
.CONVTHR
 1.0d-6
**INFO
.INFO_LINEQ
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_atoms_csr_debug.check
cat >> linsca_atoms_csr_debug.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=0
ERROR[2]="MPI Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
