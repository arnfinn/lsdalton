#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_SR_EXCHANGE.info <<'%EOF%'
   LSDALTON_SR_EXCHANGE
   -------------
   Molecule:         10 He molecules on a string on a string
   Wave Function:    HF/cc-pVTZ
   Test Purpose:     Check MBIE screening
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_SR_EXCHANGE.mol <<'%EOF%'
BASIS
cc-pVTZ
10 He molecules placed on a string
STRUCTURE IS NOT OPTIMIZED. cc-pVDZ basis 
Atomtypes=1 Nosymmetry
Charge=2. Atoms=10
H   0.0    0.0    0.0 
H   0.0   10.0    0.0 
H   0.0   20.0    0.0 
H   0.0   30.0    0.0 
H   0.0   40.0    0.0 
H   0.0   50.0    0.0 
H   0.0   70.0    0.0 
H   0.0   90.0    0.0 
H   0.0  190.0    0.0 
H   0.0  200.0    0.0 
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_SR_EXCHANGE.dal <<'%EOF%'
**GENERAL
.FORCEGCBASIS
**INTEGRALS
.MBIE
.NOLINK
.SREXC
0.001
.NOGCINTEGRALTRANSFORM
**WAVE FUNCTIONS
.HF
*DENSOPT
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_SR_EXCHANGE.check
cat >> LSDALTON_SR_EXCHANGE.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Energy
CRIT1=`$GREP "Final * HF energy\: * \-28\.6002496" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="DFT ENERGY NOT CORRECT -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
