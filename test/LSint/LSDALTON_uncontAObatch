#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_uncontAObatch.info <<'%EOF%'
   LSDALTON_uncontAObatch
   -------------
   Molecule:         HCN/cc-pVDZ
   Wave Function:    HF
   Test Purpose:     Check uncont feature of AO batch build. TK
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_uncontAObatch.mol <<'%EOF%'
BASIS
cc-pVDZ Aux=cc-pVDZ-uncontracted
1 HCN molecules placed 20 atomic units apart
STRUCTURE IS NOT OPTIMIZED. STO-2G basis 
Atomtypes=3 Nosymmetry
Charge=1. Atoms=1
H   0.0   0.0    -1.0 
Charge=7. Atoms=1
N   0.0   0.0     1.5
Charge=6. Atoms=1
C   0.0   0.0     0.0
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_uncontAObatch.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DEBUGUNCONTAOBATCH
**PROFILE
.EXCHANGE
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
H1DIAG
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_uncontAObatch.check
cat >> LSDALTON_uncontAObatch.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi
CRIT1=`$GREP "II_test_uncontAObatch successful" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="II_test_uncontAObatch not successful -"

CRIT1=`$GREP "Exchange energy, mat_dotproduct\(D,K\)\= * \-15\.53398446" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="ENERGY NOT CORRECT -"

# Memory test
#CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
#TEST[3]=`expr  $CRIT1`
#CTRL[3]=1
#ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
