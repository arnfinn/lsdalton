#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSDALTON_gen1int.info <<'%EOF%'
   LSDALTON_gen1int
   -------------
   Molecule:         SO2 and Water/(6-31G/cc-pVDZ)
   Wave Function:    HF
   Test Purpose:     Check gen1int interface
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSDALTON_gen1int.mol <<'%EOF%'
ATOMBASIS
LSint test,
============================
Atomtypes=4 Nosymmetry
Charge=8.0 Atoms=1  Bas=cc-pVDZ
O          -1.244255        0.015216        1.446161 
Charge=8.0 Atoms=2  Bas=6-31G
O           1.225543        0.015216        1.446161 
O          -0.009356        0.015216       -3.215161 
Charge=16.0 Atoms=1 Bas=6-31G
S          -0.009356        0.015216        0.723517 
Charge=1.0  Atoms=2 Bas=cc-pVDZ
H          -0.337034       -0.861284       -3.013896 
H           0.931113       -0.104860       -3.346471 
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > LSDALTON_gen1int.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**INTEGRALS
.DEBUGGEN1INT
.NOFAMILY
**WAVE FUNCTIONS
.HF
*DENSOPT
.RH
.DIIS
.START
H1DIAG
.MAXIT
1
**INFO
.DEBUG_MPI_MEM
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSDALTON_gen1int.check
cat >> LSDALTON_gen1int.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

CRIT1=`$GREP "mat\_trab * 0.3326054496" $log | wc -l`
CRIT1=`$GREP "Gen1Int * 0.3326054496" $log | wc -l`
TEST[1]=`expr   $CRIT1 \+ $CRIT1`
CTRL[1]=2
ERROR[1]="XDIPLEN NOT CORRECT"

CRIT1=`$GREP "mat\_trab * 0.1058008107" $log | wc -l`
CRIT1=`$GREP "Gen1Int * 0.1058008107" $log | wc -l`
TEST[2]=`expr   $CRIT1 \+ $CRIT1`
CTRL[2]=2
ERROR[2]="YDIPLEN NOT CORRECT"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[4]=`expr  $CRIT1`
CTRL[4]=0
ERROR[4]="MPI Memory leak -"

CRIT1=`$GREP "genS Test Complet Successful" $log | wc -l`
TEST[6]=`expr  $CRIT1`
CTRL[6]=1
ERROR[6]="genS Test Complet not Successful"

CRIT1=`$GREP " genSx Test Complet Successful" $log | wc -l`
TEST[7]=`expr  $CRIT1`
CTRL[7]=1
ERROR[7]="genSx Test NOT Complet Successful"

CRIT1=`$GREP "expvalgenSx Test Complet Successful" $log | wc -l`
TEST[8]=`expr  $CRIT1`
CTRL[8]=1
ERROR[8]="expval genSx Test NOT Complet Successful"

CRIT1=`$GREP "genh1 Test Complet Successful" $log | wc -l`
TEST[9]=`expr  $CRIT1`
CTRL[9]=1
ERROR[9]="genh1 Test NOT Complet Successful"

CRIT1=`$GREP "genh1x Matrix Test Complete Successful" $log | wc -l`
TEST[10]=`expr  $CRIT1`
CTRL[10]=1
ERROR[10]="genh1x Matrix Test NOT Complete Successful"

CRIT1=`$GREP "genh1x Test Complet Successful" $log | wc -l`
TEST[11]=`expr  $CRIT1`
CTRL[11]=1
ERROR[11]="genh1x Test NOT Complet Successful"

CRIT1=`$GREP "second_geoderiv_overlap Completed Successful" $log | wc -l`
TEST[12]=`expr  $CRIT1`
CTRL[12]=1
ERROR[12]="second_geoderiv_overlap NOT Completed Successful"

PASSED=1
for i in 1 2 3 4 6 7 8 9 10 11 12
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
