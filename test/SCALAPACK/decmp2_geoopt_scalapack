#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > decmp2_geoopt_scalapack.info <<'%EOF%'
   DEC-MP2 energy for full molecule
   --------------------------------
   Molecule:         C2H4
   Wave Function:    MP2 / STO-3G
   Test Purpose:     Test DEC-MP2 geometry optimization using SCALAPACK (Kasper K).
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > decmp2_geoopt_scalapack.mol <<'%EOF%'
BASIS
STO-3G


Atomtypes=2 Charge=0 Nosymmetry Angstrom
Charge=6.0 Atoms=2
C          0.65790       -0.00446        0.06398
C         -0.65783        0.00446       -0.06385
Charge=1.0 Atoms=4
H          1.16103        0.06607        1.02388
H          1.33519       -0.08301       -0.78143
H         -1.33549        0.08301        0.78127
H         -1.16080       -0.06606       -1.02386
%EOF%
#######################################################################
#  DALTON INPUT
#######################################################################
cat > decmp2_geoopt_scalapack.dal <<'%EOF%'
**OPTIMIZE
.LOOSE
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH DAVID
.START
TRILEVEL
.CONVDYN
TIGHT
**INFO
.DEBUG_MPI_MEM
**DEC
.FROZENCORE
.MEMORY
2.0
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >decmp2_geoopt_scalapack.check
cat >>decmp2_geoopt_scalapack.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# Final DEC-MP2 energy
CRIT1=`$GREP "Energy at final geometry is *: *\-77.195" $log | wc -l`
TEST[1]=`expr   $CRIT1`
CTRL[1]=1
ERROR[1]="Final DEC-MP2 ENERGY NOT CORRECT -"

# Memory test for total memory                                             
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`         
TEST[2]=`expr  $CRIT1`                                                    
CTRL[2]=1                                                                 
ERROR[2]="Memory leak -"

# MPI Memory test
CRIT1=`$GREP "[0-9][0-9] byte  \- Should be zero \- otherwise a leakage is present" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=0
ERROR[3]="MPI Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%




