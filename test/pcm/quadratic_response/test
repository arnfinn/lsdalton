#!/usr/bin/env python

import os
import sys
import shutil

sys.path.append(os.path.join(os.path.dirname(__file__), '../..'))
from runtest_lsdalton import Filter, TestRun

test = TestRun(__file__, sys.argv)

f_hf = Filter()
f_hf.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_hf.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_hf.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_hf.add(string = 'Final HF energy:',
      rel_tolerance = 1.0e-9)
f_hf.add(from_string = 'FIRST HYPERPOLARIZABILITY TENSOR RESULTS',
      to_string   = 'ZZZ',
      ignore_below = 1.0e-8,
      rel_tolerance = 1.0e-8)

f_hf.add(string = 'BetaPerpendicular',
      rel_tolerance = 1.0e-5)
f_hf.add(string = 'BetaParallel',
      rel_tolerance = 1.0e-5)

# First Hyperpolar HF
test.run(['beta_hf.dal'], ['CH2O.mol'], {'out': f_hf}, ['pcmsolver.pcm'])

f_hfTPA = Filter()
f_hfTPA.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_hfTPA.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_hfTPA.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_hfTPA.add(string = 'Final HF energy:',
      rel_tolerance = 1.0e-9)
f_hfTPA.add(from_string = 'TWO-PHOTON ABSORPTION RESULTS',
      num_lines   = 52,
      ignore_sign = True,
      abs_tolerance = 1.0e-4)
# Two Photon Absorption HF
test.run(['tpa_hf.dal'], ['CH2O.mol'], {'out': f_hfTPA}, ['pcmsolver.pcm'])

f_lda = Filter()
f_lda.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_lda.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_lda.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_lda.add(string = 'Final DFT energy:',
      rel_tolerance = 1.0e-9)
f_lda.add(from_string = 'FIRST HYPERPOLARIZABILITY TENSOR RESULTS',
      to_string   = 'ZZZ',
      ignore_below = 1.0e-8,
      rel_tolerance = 1.0e-8)
f_lda.add(string = 'BetaPerpendicular',
      rel_tolerance = 1.0e-5)
f_lda.add(string = 'BetaParallel',
      rel_tolerance = 1.0e-5)

# First Hyperpolar LDA
test.run(['beta_lda.dal'], ['CH2O.mol'], {'out': f_lda}, ['pcmsolver.pcm'])


f_ldaTPA = Filter()
f_ldaTPA.add(string = 'Nuclear repulsion:',
      rel_tolerance = 1.0e-9)
f_ldaTPA.add(string = 'Electronic energy:',
      rel_tolerance = 1.0e-9)
f_ldaTPA.add(string = 'PCM polarization energy:',
      abs_tolerance = 1.0e-7)
f_ldaTPA.add(string = 'Final DFT energy:',
      rel_tolerance = 1.0e-9)
f_ldaTPA.add(from_string = 'TWO-PHOTON ABSORPTION RESULTS',
      num_lines   = 52,
      ignore_sign = True,
      abs_tolerance = 1.0e-4)
# Two Photon Absorption LDA
test.run(['tpa_lda.dal'], ['CH2O.mol'], {'out': f_ldaTPA}, ['pcmsolver.pcm'])

sys.exit(test.return_code)
