#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > LSresponse_mcd.info <<'%EOF%'
   LSresponse_mcd 
   -------------
   Molecule:         HF
   Wave Function:    DFT (B3LYP) / Huckel
   Test Purpose:     Test MCD: Aterm, Bterm and Damped MCD. 
                     
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > LSresponse_mcd.mol <<'%EOF%'
BASIS
Huckel
H3+ molecule with
a Huckel basis
Atomtypes=1  Charge=1  Nosymmetry Angstrom
Charge=1.0  Atoms=3
H  0.000000     0.346410     0.000000
H  0.300000    -0.173205     0.000000
H -0.300000    -0.173205     0.000000
%EOF%

#######################################################################
#  DALTON INPUT
#  WARNING DO NOT USE THIS INPUT WITHOUT MODIFICATION 
#######################################################################
cat > LSresponse_mcd.dal <<'%EOF%'
**GENERAL
.NOGCBASIS
**WAVE FUNCTIONS
.DFT
B3LYP
*DENSOPT
.RH
.DIIS
.NVEC
8
.START
H1DIAG
.CONVTHR
1.0D-7
**RESPONSE
*QUASIMCD
.DEGENERATE
.MCDEXCIT
2
.GAUSSIAN
.LINESHAPEPARAM
0.002D0
.NVECFORPEAK
3
.NSTEPS
1000
.NO DAMPED
*END QUASIMCD
*SOLVER
.NOPREC
.CONVTHR
1.0D-6
.MAXIT
40
.MAXRED
80
*END OF INPUT
%EOF%
#######################################################################

 

#######################################################################

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >LSresponse_mcd.check
cat >>LSresponse_mcd.check <<'%EOF%'

log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# ENERGY                                         
CRIT1=`$GREP "This is a DFT calculation of type: B3LYP" $log | wc -l`  
CRIT2=`$GREP "Final DFT energy\: *\-1\.001861447" $log | wc -l`       
TEST[1]=`expr   $CRIT1 + $CRIT2`                        
CTRL[1]=2                                                           
TEST[1]=`expr   $CRIT2`                        
CTRL[1]=1                                                           
ERROR[1]="DFT ENERGY NOT CORRECT -"  

# A term
CRIT1=`$GREP "london A term \(a\.u\.\) * 0?\.4807" $log | wc -l` 
CRIT2=`$GREP "london A term \(std unit * 6?\.2113" $log | wc -l` 
CRIT3=`$GREP "nonlondon A term \(a\.u\.\) * 0?\.1600" $log | wc -l` 
CRIT4=`$GREP "nonlondon A term \(std unit * 2?\.0679" $log | wc -l` 
TEST[2]=`expr   $CRIT1 + $CRIT2 + $CRIT3 + $CRIT4`     
CTRL[2]=4   
ERROR[2]="Aterm NOT CORRECT -"     

# B term
CRIT1=`$GREP "LAO    BTERM\-result  \(a\.u\.\) * : * 0?\.3413" $log | wc -l` 
CRIT2=`$GREP "LAO    BTERM\-result  \(std units\) * : * 0?\.0000" $log | wc -l` 
CRIT3=`$GREP "NONLAO BTERM\-result  \(a\.u\.\) * : * 0?\.0000" $log | wc -l` 
TEST[3]=`expr   $CRIT1 + $CRIT2 + $CRIT3`     
CTRL[3]=12
ERROR[3]="Bterm NOT CORRECT -"     

# Excitation
CRIT1=`$GREP "Excitation Energies\(au\)\: * 2.35270" $log | wc -l` 
TEST[4]=`expr   $CRIT1`     
CTRL[4]=2 
ERROR[4]="Excitation Energies NOT CORRECT -"     

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[5]=`expr  $CRIT1`
CTRL[5]=1
ERROR[5]="Memory leak -"

PASSED=1
for i in 1 2 3 4 5
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
     echo ${ERROR[i]}
     PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
  echo TEST ENDED PROPERLY
  exit 0
else
  echo THERE IS A PROBLEM
  exit 1
fi

%EOF%
#######################################################################
