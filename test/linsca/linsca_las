#!/bin/sh
#
# This is the script for generating files for a specific Dalton test job.
#
# For the .check file ksh or bash is preferred, otherwise use sh
# (and hope it is not the old Bourne shell, which will not work)
#
if [ -x /bin/ksh ]; then
   CHECK_SHELL='#!/bin/ksh'
elif [ -x /bin/bash ]; then
   CHECK_SHELL='#!/bin/bash'
else
   CHECK_SHELL='#!/bin/sh'
fi


#######################################################################
#  TEST DESCRIPTION
#######################################################################
cat > linsca_las.info <<'%EOF%'
   linsca_las
   ---------------
   Molecule:         C10H2
   Wave Function:    HF/3-21G
   Test Purpose:     Check the Low Accuracy start keyword
%EOF%

#######################################################################
#  MOLECULE INPUT
#######################################################################
cat > linsca_las.mol <<'%EOF%'
BASIS
6-31G*
C10H2

Atomtypes=2 Nosymmetry Angstrom
Charge=6. Atoms=10
C         0.00000000      0.00000000      1.08290700
C         0.00000000      0.00000000      2.29461600
C         0.00000000      0.00000000      3.64429100
C         0.00000000      0.00000000      4.86890900
C         0.00000000      0.00000000      6.20522000
C         0.00000000      0.00000000      7.43493100
C         0.00000000      0.00000000      8.76635300
C         0.00000000      0.00000000      9.99834400
C         0.00000000      0.00000000     11.32744700
C         0.00000000      0.00000000     12.56059500
Charge=1. Atoms=2
H         0.00000000      0.00000000      0.00000000
H         0.00000000      0.00000000     14.02766500
%EOF%

#######################################################################
#  DALTON INPUT
#######################################################################
cat > linsca_las.dal <<'%EOF%'
**GENERAL
.TESTMPICOPY
**WAVE FUNCTIONS
.HF
*DENSOPT
.ARH
.START
ATOMS
.LOW ACCURACY START
.CONVTHR
2.0d-3
.NO HLGAP
*END OF INPUT
%EOF%

#######################################################################
#  CHECK SCRIPT
#######################################################################
echo $CHECK_SHELL >linsca_las.check
cat >> linsca_las.check <<'%EOF%'
log=$1

if [ `uname` = Linux ]; then
   GREP="egrep -a"
else
   GREP="egrep"
fi

# test energy
CRIT1=`$GREP "Final HF energy: * \-379\.4495" $log | wc -l`
TEST[1]=`expr  $CRIT1`
CTRL[1]=1
ERROR[1]="HF ENERGY NOT CORRECT -"


# Test use of keyword
CRIT1=`$GREP "use Low accuracy start settings" $log | wc -l`
TEST[2]=`expr  $CRIT1`
CTRL[2]=1
ERROR[2]="do not use the Low accuracy start keyword -"

# Memory test
CRIT1=`$GREP "Allocated memory \(TOTAL\): * 0 byte" $log | wc -l`
TEST[3]=`expr  $CRIT1`
CTRL[3]=1
ERROR[3]="Memory leak -"

PASSED=1
for i in 1 2 3
do
   if [ ${TEST[i]} -ne ${CTRL[i]} ]; then
      echo ${ERROR[i]}
      PASSED=0
   fi
done

if [ $PASSED -eq 1 ]
then
   echo TEST ENDED PROPERLY
   exit 0
else
   echo THERE IS A PROBLEM
   exit 1
fi

%EOF%
#######################################################################
