!> @file 
!> Contains DSM extra term module.

!> Module for adding an extra, more expensive, term to the DSM function.
module dsm_x_term
   private
contains
  subroutine dsm_xterm_dummy()
    implicit none
  end subroutine dsm_xterm_dummy

end module dsm_x_term
