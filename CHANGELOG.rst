=========
ChangeLog
=========

Unreleased
----------

Added
+++++

- Tests on PCM static and dynamic first hyperpolarizability
- Tests on PCM Two Photon Absorption
- Tests on PCM static and dynamic second hyperpolarizability (only HF)

Changed
+++++++

- Docker images for CI. The image now used is based on Ubuntu 16.04 and
  includes GCC 5.3.1, OpenMPI 1.10 and MKL 2017.4.239.
  The Dockerfile can be found `here <https://github.com/robertodr/dockerfiles/blob/master/Dockerfile.ubuntu-16.04_gcc5.3.1_openmpi1.10_mkl2017.4.239>`_.

1.2 (2016-02-08)
----------------

- A bug in using cartesian basis functions was identified and fixed.

1.1 (2016-01-13)
----------------

- The "make install" was fixed.

1.0 (2015-12-22)
----------------

See the release notes at http://daltonprogram.org for a list of new features in
Dalton and LSDalton.
